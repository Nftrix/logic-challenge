var express = require("express");
var app = express();
var submission = require("./modules/submission");
var admin = require("./modules/admin");
var template = require("./modules/_template");
var about = require("./modules/about");
var dashboard = require("./modules/dashboard");

var now = require("moment");


'use strict';

app.listen(process.env.PORT || 8080, function(){
   console.log("---Application Starting on port "+process.env.PORT+"---");
});



app.set('view engine', 'ejs');
app.set('views', ['./modules/submission','./modules/admin', './modules/_template', './modules/about']);

app.use('/static', express.static('public'));

app.use(function timeLog (req, res, next) {
  console.log(req.method,'Request received from ',req.ip,' for page ',req.url,' at time: ', now());
  next();
});

app.use("/", submission);

app.use("/admin", admin);

app.use("/template", template);

app.use("/about", about);

app.use("/dashboard", dashboard);