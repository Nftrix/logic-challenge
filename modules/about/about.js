var express = require("express");
var router = express.Router();

'use strict';


function render (req, res){
     res.status(200).render('about');
}

module.exports = {render};