var express = require("express");
var router = express.Router();
var about = require("./about");

'use-strict';

// define the home page route
router.route('/')
    .get(about.render);


module.exports = [router];