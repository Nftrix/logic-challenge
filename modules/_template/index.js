var express = require("express");
var router = express.Router();
var template = require("./_template");

'use-strict';

// define the home page route
router.route('/')
    .get(template.render);


module.exports = [router];