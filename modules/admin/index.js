var express = require("express");
var router = express.Router();
var admin = require("./admin");

'use-strict';

// define the home page route
router.route('/')
    .get(admin.render);


module.exports = [router];