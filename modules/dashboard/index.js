var express = require("express");
var router = express.Router();
var dashboard = require("./dashboard");

'use-strict';

// define the home page route
// router.route('/')
//     .get(dashboard.render);

router.route('/showallanwsers')
    .get(dashboard.showAllAnswers);

module.exports = [router];