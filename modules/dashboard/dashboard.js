var fs = require("fs");

'use strict';


function render (req, res){
     res.status(200).send('dashboard');
}

function showAllAnswers(req, res) {
    var answersList = 'data/answers.txt';
    fs.readFile(answersList, (err, data) => {
        if (err) throw err;
        res.status(200).send(data);
    });
}


module.exports = {render, showAllAnswers};