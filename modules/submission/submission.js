// var express = require("express");
var fs = require("fs");

'use strict';

function render(req, res) {
    var questionList = 'data/questions.txt';
    fs.readFile(questionList, (err, data) => {
        if (err) throw err;
        //console.log(JSON.parse(data));
        var activeQuestion = (JSON.parse(data)).filter(function(data){
            return data.shown === "false" &&
                data.active === "true";
        });
        console.log(activeQuestion);
        res.status(200).render('submission',{activeQuestion});
    });
    
}


function submitAnswer(req, res) {
    var answersList = 'data/answers.txt';
    res.status(200).render('submitted',{});
}




module.exports = {render,submitAnswer};
