var express = require("express");
var router = express.Router();
var submissions = require("./submission");

'use-strict';

// define the home page route
router.route('/')
    .get(submissions.render)
    .post(submissions.submitAnswer);



module.exports = [router];